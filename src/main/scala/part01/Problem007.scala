package part01

/**
 * 10001st prime
 * Problem 7
 *
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 *
 * What is the 10 001st prime number?
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler007
 */
object Problem007 {

  lazy val primes = BigInt(2) #:: sieve(BigInt(3))

  def sieve(n: BigInt): Stream[BigInt] =
    if (primes.takeWhile(_.pow(2) <= n).exists(n % _ == 0)) sieve(n + 2)
    else n #:: sieve(n + 2)

  def nthPrime(n: Int): BigInt = primes(n-1)

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    val n = line.toInt
    val result = nthPrime(n)
    println(result)
  }
}