package part01

/**
 * Factorial digit sum
 * Problem 20
 *
 * n! means n x (n - 1) x ... x 3 x 2 x 1
 *
 * For example, 10! = 10 x 9 x ... x 3 x 2 x 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 *
 * Find the sum of the digits in the number 100!
 */
object Problem020 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val factorials: Stream[BigInt] = BigInt(1) #:: factorials.zip(N).map(a => a._1 * a._2)

  implicit class Factorial(val n: Int) extends AnyVal {
    def ! : BigInt = {
      factorials.take(n + 1).last
    }
  }

  val result = (100!).toString.map(_.asDigit).sum
  println(result)

}