package part02

/**
 * Non-abundant sums
 * Problem 23
 *
 * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
 * For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means
 * that 28 is a perfect number.
 *
 * A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
 *
 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant
 * numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two
 * abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest
 * number that cannot be expressed as the sum of two abundant numbers is less than this limit.
 *
 * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 */
object Problem023 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  def properDivisors(n: BigInt): Set[BigInt] = {
    val dividors = N.takeWhile(m => m * m < n + 1).filter(n % _ == 0).map(x => List(x, n / x)).flatten.toSet
    dividors diff Set(n)
  }

  def isAbundant(n: BigInt): Boolean = properDivisors(n).sum > n

  val abundantNumbers: Stream[BigInt] = N.filter(isAbundant(_))

  val result = (BigInt(1) to 28123).filter {
    n => abundantNumbers.takeWhile(_ <= n / 2).find(m => isAbundant(n - m)).isEmpty
  }.sum
  println(result)

}