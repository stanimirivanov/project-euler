package part02

/**
 * Champernowne's constant
 * Problem 40
 *
 * An irrational decimal fraction is created by concatenating the positive integers:
 *
 * 			0.123456789101112131415161718192021...
 * 						 |
 *
 * It can be seen that the 12th digit of the fractional part is 1.
 *
 * If dn represents the n-th digit of the fractional part, find the value of the following expression.
 *
 * d1 x d10 x d100 x d1000 x d10000 x d100000 x d1000000
 */
object Problem040 extends App {

  lazy val N: Stream[BigInt] = BigInt(0) #:: N.map(_ + 1)

  lazy val champernowne: Stream[Int] = N.flatMap {
    n => n.toString.map(_.asDigit)
  }

  def d(n: Int): BigInt = champernowne.drop(n).head

  val result = d(1) * d(10) * d(100) * d(1000) * d(10000) * d(100000) * d(1000000)
  println(result)

}