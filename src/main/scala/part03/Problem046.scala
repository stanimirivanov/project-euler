package part03

/**
 * Goldbach's other conjecture
 * Problem 46
 *
 * It was proposed by Christian Goldbach that every odd composite number
 * can be written as the sum of a prime and twice a square.
 *
 * 9 = 7 + 2x12
 * 15 = 7 + 2x22
 * 21 = 3 + 2x32
 * 25 = 7 + 2x32
 * 27 = 19 + 2x22
 * 33 = 31 + 2x12
 *
 * It turns out that the conjecture was false.
 *
 * What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
 */
object Problem046 extends App {

  lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)

  lazy val composites = BigInt(2) #:: ODD.filter(n => !n.isProbablePrime(100))

  lazy val primes = BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))
  
  def square(n: BigInt) = n * n

  def isSquare(n: BigInt) = n == square(math.round(math.sqrt(n.toDouble)))
  
  def isGoldbach(n: BigInt) =
    primes.takeWhile(_ < n).exists {
      p => isSquare((n - p) / 2)
    }
  
  val result = composites.dropWhile(_ < 3).filter(n => !isGoldbach(n)).head
  println(result)
  
}