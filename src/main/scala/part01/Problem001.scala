package part01

/**
 * Multiples of 3 and 5
 * Problem 1
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below N.
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler001
 */
object Problem001 {

  def multiplesBelow(limit: Int): BigInt =
    sequenceSum(3, limit) + sequenceSum(5, limit) - sequenceSum(15, limit)

  def sequenceSum(start: BigInt, end: BigInt): BigInt = {
    val count = (end - 1) / start
    start * count * ( count + 1) / 2
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    val result = multiplesBelow(line.toInt)
    println(result)
  }

}