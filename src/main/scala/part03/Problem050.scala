package part03

/**
 * Consecutive prime sum
 * Problem 50
 *
 * The prime 41, can be written as the sum of six consecutive primes:
 *
 * 41 = 2 + 3 + 5 + 7 + 11 + 13
 *
 * This is the longest sum of consecutive primes that adds to a prime below one-hundred.
 *
 * The longest sum of consecutive primes below one-thousand that adds to a prime,
 * contains 21 terms, and is equal to 953.
 *
 * Which prime, below one-million, can be written as the sum of the most consecutive primes?
 */
object Problem050 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)

  lazy val primes = BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))

  val candidates = primes.takeWhile(_ < 1000000).toList

  val upperLimit = N.indexWhere(i => primes.take(i.toInt).sum > 1000000)

  def slidings(size: Int): Iterator[List[BigInt]] =
    candidates.sliding(size, 1).takeWhile(_.sum < 1000000)

  val result = (upperLimit to 1 by -1).flatMap(slidings(_)).map(_.sum).find(_.isProbablePrime(100)).get
  println(result)

}