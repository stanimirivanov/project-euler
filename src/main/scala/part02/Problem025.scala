package part02

/**
 * 1000-digit Fibonacci number
 * Problem 25
 * 
 * The Fibonacci sequence is defined by the recurrence relation:
 *
 * Fn = Fn_1 + Fn_2, where F1 = 1 and F2 = 1.
 * Hence the first 12 terms will be:
 *
 * F1 = 1
 * F2 = 1
 * F3 = 2
 * F4 = 3
 * F5 = 5
 * F6 = 8
 * F7 = 13
 * F8 = 21
 * F9 = 34
 * F10 = 55
 * F11 = 89
 * F12 = 144
 * 
 * The 12th term, F12, is the first term to contain three digits.
 *
 * What is the first term in the Fibonacci sequence to contain 1000 digits?
 */
object Problem025 extends App {

  lazy val fibonacci: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fibonacci.zip(fibonacci.tail).map(p => p._1 + p._2)
  
  val indexedFibonacci = fibonacci.zipWithIndex.dropWhile(p => p._1.toString.length() < 1000).head
  val result = indexedFibonacci._2
  println(result)
  
}