package part02

/**
 * Double-base palindromes
 * Problem 36
 *
 * The decimal number, 585 = 1001001001_2 (binary), is palindromic in both bases.
 *
 * Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
 *
 * (Please note that the palindromic number, in either base, may not include leading zeros.)
 */
object Problem036 extends App {
  
  def isPalindrome(n: String) = n.reverse.sameElements(n)

  def isDoubleBasePalindrome(n: Int) = isPalindrome(n.toString) && isPalindrome(n.toBinaryString)
  
  def result = (1 until 1E6.toInt by 2).filter(n => isDoubleBasePalindrome(n)).sum
  println(result)

}
