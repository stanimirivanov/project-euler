package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem009Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem009.tripletProduct(1000) }
    assert(result === Some(31875000))
  }

  "Problem" should "pass max value" in {
    val result = time { Problem009.tripletProduct(3000) }
    assert(result === Some(937500000))
  }

}