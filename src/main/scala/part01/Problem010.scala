package part01

/**
 * Summation of primes
 * Problem 10
 *
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 *
 */
object Problem010 extends App {

  def primesUntil(m: BigInt): Stream[BigInt] = {
    lazy val primes = BigInt(2) #:: sieveOfEratosthenes(BigInt(3))

    def sieveOfEratosthenes(n: BigInt): Stream[BigInt] = {
      if (primes.takeWhile(p => p * p <= n).exists(n % _ == 0))
        sieveOfEratosthenes(n + 2)
      else n #:: sieveOfEratosthenes(n + 2)
    }

    primes.takeWhile(_ < m)
  }

  val result = primesUntil(BigInt(2000000)).sum
  println(result)
}