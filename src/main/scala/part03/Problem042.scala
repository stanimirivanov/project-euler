package part03

import scala.io._
import scala.util._

/**
 * Coded triangle numbers
 * Problem 42
 *
 * The n-th term of the sequence of triangle numbers is given by, tn = 1/2n(n+1); so the first ten triangle numbers are:
 *
 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
 *
 * By converting each letter in a word to a number corresponding to its alphabetical position and adding these values
 * we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t_10. If the word value is a
 * triangle number then we shall call the word a triangle word.
 *
 * Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common
 * English words, how many are triangle words?
 * 
 * @see http://www.mathblog.dk/project-euler-42-triangle-words/
 */
object Problem042 extends App {

  val fileName = "words.txt"

  def readTextFile(fileName: String): Try[Array[String]] = {
    Try(Source.fromFile(fileName).mkString.replaceAll("\"", "").split(","))
  }

  def wordValue(w: String) = w.foldLeft(0)((s, l) => (s + (l - 'A' + 1)))

  def isTriangular(n: Int): Boolean = (scala.math.sqrt(8 * n + 1) - 1) % 2 == 0

  readTextFile(fileName) match {
    case Success(lines) => {
      val result = lines.count(w => isTriangular(wordValue(w)))
      println(result)
    }
    case Failure(f) => println(f)
  }

}