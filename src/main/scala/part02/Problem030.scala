package part02

/**
 * Digit fifth powers
 * Problem 30
 *
 * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
 *
 * 1634 = 1^4 + 6^4 + 3^4 + 4^4
 * 8208 = 8^4 + 2^4 + 0^4 + 8^4
 * 9474 = 9^4 + 4^4 + 7^4 + 4^4
 *
 * As 1 = 1^4 is not a sum it is not included.
 *
 * The sum of these numbers is 1634 + 8208 + 9474 = 19316.
 *
 * Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
 */
object Problem030 extends App {

  lazy val N: Stream[BigInt] = BigInt(0) #:: N.map(_ + 1)

  def isFifthPowersNumber(n: BigInt): Boolean =
    n.toString.toList.map(_.asDigit).map(scala.math.pow(_, 5).toInt).sum == n

  // search up to 7 * 9^5 since no seven digit number can have a power sum that is seven digits
  val limit = 7 * scala.math.pow(9, 5).toInt

  val result = N.drop(2).takeWhile(_ < limit).filter(isFifthPowersNumber(_)).sum
  println(result)

}