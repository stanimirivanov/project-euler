package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem002Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem002.sumEvenFibonacci(4000000) }
    assert(result === BigInt(4613732))
  }

  "Problem" should "pass max value" in {
    val result = time { Problem002.sumEvenFibonacci(BigInt("40000000000000000")) }
    assert(result === BigInt("49597426547377748"))
  }

}