package part02

/**
 * Digit cancelling fractions
 * Problem 33
 *
 * The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may
 * incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
 *
 * We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
 *
 * There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing
 * two digits in the numerator and denominator.
 *
 * If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
 * 
 * @see http://www.mathblog.dk/project-euler-33-fractions-unorthodox-cancelling-method/
 */
object Problem033 extends App {

  def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  val fractions = for {
    i <- (1 to 9);
    d <- (1 to i);
    n <- (1 to d);
    if ((n * 10 + i) * d == n * (i * 10 + d) && d != n)
  } yield (n, d)
  
  val nominatorsProduct = fractions.map(_._1).product
  val denominatorsProduct = fractions.map(_._2).product

  val result = denominatorsProduct / gcd(nominatorsProduct, denominatorsProduct)
  println(result)

}