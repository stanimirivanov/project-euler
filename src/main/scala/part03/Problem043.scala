package part03

/**
 * Sub-string divisibility
 * Problem 43
 *
 * The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in
 * some order, but it also has a rather interesting sub-string divisibility property.
 *
 * Let d_1 be the 1-st digit, d_2 be the 2-nd digit, and so on. In this way, we note the following:
 *
 * d_2 d_3 d_4 = 406 is divisible by 2
 * d_3 d_4 d_5 = 063 is divisible by 3
 * d_4 d_5 d_6 = 635 is divisible by 5
 * d_5 d_6 d_7 = 357 is divisible by 7
 * d_6 d_7 d_8 = 572 is divisible by 11
 * d_7 d_8 d_9 = 728 is divisible by 13
 * d_8 d_9 d_10 = 289 is divisible by 17
 *
 * Find the sum of all 0 to 9 pandigital numbers with this property.
 */
object Problem043 extends App {

  val divisors = List(2, 3, 5, 7, 11, 13, 17)

  def isSubStringPandigital(n: BigInt): Boolean = {
    val subStrings: List[String] = n.toString.tail.sliding(3, 1).toList
    subStrings.zip(divisors).forall {
      case (subString, p) => subString.toInt % p == 0
    }
  }

  val permutations = (0 to 9).permutations.filterNot(_.head == 0)
  val result = permutations.map(n => BigInt.apply(n.mkString)).filter(isSubStringPandigital(_)).sum
  println(result)

}