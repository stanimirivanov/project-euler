package part01

/**
 * Smallest multiple
 * Problem 5
 *
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to N?
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler005
 */
object Problem005 {

  def smallestNumberDivisibleInRange(start: Long, end: Long): Long = {
    def gcd(a: Long, b: Long): Long = {
      val r = a % b
      if (r == 0) b else gcd(b, r)
    }

    def lcm(a: Long, b: Long) = (a * b) / gcd(a, b)

    (start to end) reduce { (a, b) => lcm(a, b) }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail foreach { printResult }
  }

  def printResult(line: String): Unit = {
    val limit = line.toLong
    val result = smallestNumberDivisibleInRange(1L, limit)
    println(result)
  }

}