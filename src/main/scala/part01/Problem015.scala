package part01

/**
 * Longest Collatz sequence
 * Problem 14
 *
 * The following iterative sequence is defined for the set of positive integers:
 *
 * n -> n/2 (n is even)
 * n -> 3n + 1 (n is odd)
 *
 * Using the rule above and starting with 13, we generate the following sequence:
 *
 * 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
 * Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
 *
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 * 
 * Alternatively, the answer is the Pascal triangle number C(2n, n) = 2n!/n!^2
 */
object Problem015 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val factorials: Stream[BigInt] = BigInt(1) #:: factorials.zip(N).map(a => a._1 * a._2)

  implicit class Factorial(val n: Int) extends AnyVal {
    def ! : BigInt = {
      factorials.take(n + 1).last
    }
  }

  val result = (40!) / ((20!) * (20!))
  println(result)
}