package part03

/**
 * Self powers
 * Problem 48
 *
 * The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
 *
 * Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
 */
object Problem048 extends App {

  val M = BigInt(10).pow(10)
  
  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)
  
  val result = N.takeWhile(_ <= 1000).map(n => n.pow(n.toInt)).sum % M
  println(result)
  
  // improved modulo solution 
  
  val resultAlt = N.takeWhile(_ <= 1000).map(n => n.modPow(n, M)).sum % M
  println(resultAlt)
  
}