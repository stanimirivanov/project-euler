package part02

import scala.collection.immutable.NumericRange


/**
 * Amicable numbers
 * Problem 21
 *
 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair and each of a and b are called amicable numbers.
 *
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
 * The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 *
 * Evaluate the sum of all the amicable numbers under 10000.
 */
object Problem021 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  def properDivisors(n: BigInt): Set[BigInt] = {
    val divisors = N.takeWhile(m => m * m < n + 1).filter(n % _ == 0).map(x => List(x, n / x)).flatten.toSet
    divisors diff Set(n)
  }
  
  def d(n: BigInt): BigInt = properDivisors(n).sum

  def isAmicable(a: BigInt): Boolean = {
    val b = d(a)
    a != b && d(b) == a
  }

  def amicablePairs(range: NumericRange[BigInt]): IndexedSeq[BigInt] =
    range.filter(a => isAmicable(a))

  val result = amicablePairs(BigInt(1) until 10000).sum
  println(result)

}