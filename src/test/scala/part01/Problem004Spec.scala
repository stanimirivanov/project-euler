package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem004Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem004.palindromicNumbers.max }
    assert(result === 906609)
  }

  "Problem" should "pass max value" in {
    val result = time { Problem004.largestPalindromicNumber(1000000) }
    assert(result === 906609)
  }

}