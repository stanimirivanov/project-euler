package part02

/**
 * Circular primes
 * Problem 35
 *
 * The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
 *
 * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
 *
 * How many circular primes are there below one million?
 */
object Problem035 extends App {
  
  def rotations(n: BigInt): Seq[BigInt] = {
    val s = n.toString
    (1 to s.size - 1).map(i => { val intValue = List(s.drop(i), s.take(i)).mkString.toInt; BigInt(intValue) })
  }

  val circularPrimes = (BigInt(2) until 1000000).filter(n => n.isProbablePrime(100) && rotations(n).forall(_.isProbablePrime(100)))
  val result = circularPrimes.size
  println(result)

}
