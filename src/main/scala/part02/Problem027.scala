package part02

/**
 * Quadratic primes
 * Problem 27
 * 
 * Euler discovered the remarkable quadratic formula:
 *
 * n^2 + n + 41
 *
 * It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39.
 * However, when n = 40, 40^2 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and
 * certainly when n = 41, 41^2 + 41 + 41 is clearly divisible by 41.
 *
 * The incredible formula  n^2 - 79n + 1601 was discovered, which produces 80 primes for
 * the consecutive values n = 0 to 79. The product of the coefficients, -79 and 1601, is -126479.
 *
 * Considering quadratics of the form:
 *
 * n^2 + an + b, where |a| < 1000 and |b| < 1000
 *
 * where |n| is the modulus/absolute value of n
 * e.g. |11| = 11 and |-4| = 4
 *
 * Find the product of the coefficients, a and b, for the quadratic expression that produces
 * the maximum number of primes for consecutive values of n, starting with n = 0.
 */
object Problem027 extends App {

  lazy val N: Stream[BigInt] = BigInt(0) #:: N.map(_ + 1)

  lazy val primes = {
    lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)
    BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))
  }

  def f(n: BigInt, a: BigInt, b: BigInt): BigInt = n * n + a * n + b

  def quadraticPrimes(a: BigInt, b: BigInt): Stream[BigInt] =
    N.takeWhile(n => primes.takeWhile(_ < 1000).contains(f(n, a, b)))

  val resultTuples = for {
    b <- primes.takeWhile(_ < 1000)
    a <- BigInt(-999) to BigInt(999)
  } yield (a, b, quadraticPrimes(a, b))

  val maxResult = resultTuples.maxBy(abqPrimes => { val (a, b, qPrimes) = abqPrimes; qPrimes.lastOption.getOrElse(BigInt(0)) })
  val result = maxResult._1 * maxResult._2
  println(result)

}