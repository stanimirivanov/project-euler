package utils

import java.util.concurrent.TimeUnit

/**
 * Profiler for tests.
 *
 * @see http://stackoverflow.com/questions/9160001/how-to-profile-methods-in-scala
 */
trait Profiler {
  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    val elapsedTime = (t1 - t0)

    val seconds = TimeUnit.NANOSECONDS.toSeconds(elapsedTime)
    val millis = TimeUnit.NANOSECONDS.toMillis(elapsedTime)
    val time = if (millis == 0) elapsedTime + " ns"
      else if (seconds == 0) millis + " ms"
      else seconds + " s"
    println(getClass.getName + " answer: " + result + " ... in " + time)
    result
  }
}
