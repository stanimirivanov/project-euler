package part02

/**
 * Integer right triangles
 * Problem 39
 *
 * If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.
 *
 * {20,48,52}, {24,45,51}, {30,40,50}
 *
 * For which value of p <= 1000, is the number of solutions maximised?
 * 
 * @see http://www.mathblog.dk/project-euler-39-perimeter-right-angle-triangle/
 */
object Problem039 extends App {

  def isIntegerRightTriangle(a: Int, b: Int, c: Int): Boolean = a * a + b * b == c * c

  def integerRightTriangles(p: Int): Seq[(Int, Int, Int)] = {
    for {
      a <- (1 to p / 3);
      b <- (a to p / 2);
      c =  p - a - b;
      if (isIntegerRightTriangle(a, b, c))
    } yield (a, b, c)
  }

  val result = (2 to 1000 by 2).maxBy(p => integerRightTriangles(p).size)
  println(result)

}