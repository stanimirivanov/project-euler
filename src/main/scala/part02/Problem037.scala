package part02

/**
 * Truncatable primes
 * Problem 37
 *
 * The number 3797 has an interesting property.
 * Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7.
 * Similarly we can work from right to left: 3797, 379, 37, and 3.
 *
 * Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
 *
 * NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
 */
object Problem037 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  def slices(n: BigInt): Seq[BigInt] = {
    val s = n.toString
    (1 to s.size - 1).map(i => { val intValue = s.take(i).toInt; BigInt(intValue) }) ++
      (1 to s.size - 1).map(i => { val intValue = s.drop(i).toInt; BigInt(intValue) })
  }

  val result = N.drop(10).filter(n => n.isProbablePrime(100) && slices(n).forall(m => m.isProbablePrime(100))).take(11).sum
  println(result)

}
