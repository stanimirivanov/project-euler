package part02

/**
 * Reciprocal cycles
 * Problem 26
 *
 * A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:
 *
 * 1/2	= 	0.5
 * 1/3	= 	0.(3)
 * 1/4	= 	0.25
 * 1/5	= 	0.2
 * 1/6	= 	0.1(6)
 * 1/7	= 	0.(142857)
 * 1/8	= 	0.125
 * 1/9	= 	0.(1)
 * 1/10	= 	0.1
 *
 * Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.
 *
 * Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
 */
object Problem026 extends App {

  lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)

  lazy val primes = BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))

  def factors(n: BigInt): List[BigInt] = primes.takeWhile(_ < n).toList

  /**
   * @see http://en.wikipedia.org/wiki/Cyclic_number
   */
  def unitFraction(p: Int, r: Int = 1, t: Int = 1): List[Int] = {
    val x = r * 10
    val digit = (x / p).intValue
    val remain = x % p
    if (remain == 1) List(digit) else digit :: unitFraction(p, remain, t + 1)
  }

  val maxDenominator = 1000
  val result = factors(maxDenominator).reverse.find {
    d => unitFraction(d.toInt).length == d - 1
  }
  println(result.getOrElse("Error computing recurring digital representations"))
}