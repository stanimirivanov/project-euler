package part03

/**
 * Pandigital prime
 * Problem 41
 *
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once.
 * For example, 2143 is a 4-digit pandigital and is also prime.
 *
 * What is the largest n-digit pandigital prime that exists?
 */
object Problem041 extends App {

  lazy val pandigitals: Seq[BigInt] = (1 to 9).flatMap {
    (1 to 9).take(_).permutations
  }.map(_.mkString).map(BigInt.apply(_))

  val result = pandigitals.filter(n => n.isProbablePrime(100)).max
  println(result)
  
}