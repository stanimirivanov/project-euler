package part01

/**
 * Largest prime factor
 * Problem 3
 *
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number N?
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler003
 */
object Problem003 {

  def factors(n: Long): List[Long] = {
    val exists = (2L to math.sqrt(n.toDouble).toLong) find { n % _ == 0 }
    exists match {
      case Some(d) => d :: factors(n/d)
      case None => List(n)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    val n = line.toLong
    val result = factors(n).max
    println(result)
  }

}