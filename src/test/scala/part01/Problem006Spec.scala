package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem006Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem006.sumSquareDifference(100) }
    assert(result === 25164150)
  }

  "Problem" should "pass max value" in {
    val result = time { Problem006.sumSquareDifference(10000) }
    assert(result === BigInt("2500166641665000"))
  }

}