package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem005Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem005.smallestNumberDivisibleInRange(1, 20) }
    assert(result === 232792560L)
  }

  "Problem" should "pass max value" in {
    val result = time { Problem005.smallestNumberDivisibleInRange(1, 40) }
    assert(result === 5342931457063200L)
  }

}