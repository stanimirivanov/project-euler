package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem001Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem001.multiplesBelow(1000) }
    assert(result === BigInt(233168))
  }

  "Problem" should "pass max value" in {
    val result = time { Problem001.multiplesBelow(1000000000) }
    assert(result === BigInt("233333333166666668"))
  }

}