package part01

/**
 * Power digit sum
 * Problem 16
 *
 * 215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 *
 * What is the sum of the digits of the number 2^1000?
 */
object Problem016 extends App {

  val result = BigInt(2).pow(1000).toString.map(s => BigInt(s.toString)).sum
  println(result)
}