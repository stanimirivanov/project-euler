package part02

/**
 * Coin sums
 * Problem 31
 *
 * In England the currency is made up of pound, $, and pence, p, and there are eight coins in general circulation:
 *
 * 		1p, 2p, 5p, 10p, 20p, 50p, $1 (100p) and $2 (200p).
 *
 * It is possible to make $2 in the following way:
 *
 * 		1x$1 + 1x50p + 2x20p + 1x5p + 1x2p + 3x1p
 *
 * How many different ways can $2 be made using any number of coins?
 */
object Problem031 extends App {

  val coeficients = for (
    a <- 0 to 200;
    b <- 0 to 100;
    c <- 0 to 40;
    d <- 0 to 20;
    e <- 0 to 10;
    f <- 0 to 4;
    g <- 0 to 2;
    h <- 0 to 1;
    if (a + 2*b + 5*c + 10*d + 20*e + 50*f + 100*g + 200*h == 200)
  ) yield (a, b, c, d, e, f, g, h)

  val result = coeficients.size
  println(result)

}