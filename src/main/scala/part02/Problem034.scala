package part02

/**
 * Digit factorials
 * Problem 34
 *
 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
 *
 * Find the sum of all numbers which are equal to the sum of the factorial of their digits.
 *
 * Note: as 1! = 1 and 2! = 2 are not sums they are not included.
 *
 * @see http://mathworld.wolfram.com/Factorion.html
 *
 * @see http://mathworld.wolfram.com/FactorialSums.html
 */
object Problem034 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val factorials: Stream[BigInt] = BigInt(1) #:: factorials.zip(N).map(a => a._1 * a._2)

  implicit class Factorial(val n: Int) extends AnyVal {
    def ! : BigInt = {
      factorials.take(n + 1).last
    }
  }

  def isFactorion(n: BigInt): Boolean = n.toString.map(d => (d.asDigit !)).sum == n

  // factorion of an n-digit number cannot exceed n * 9!
  val limit: Int = 10 * (9!).toInt
  val result = (10 until limit).filter(isFactorion(_)).sum
  println(result)

}
