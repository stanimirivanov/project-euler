package part01

/**
 * Largest palindrome product
 * Problem 4
 *
 * A palindromic number reads the same both ways.
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.
 *
 * The smallest 6 digit palindrome made from the product of two 3-digit numbers is 101101 = 143 x 707.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers which is less than N.
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler004
 */
object Problem004 {

  def largestPalindromicNumber(limit: Int): Int =
    palindromicNumbers.sorted.takeWhile(_ <= limit).max

  def palindromicNumbers: List[Int] = {
    def isPalindrome(s: String): Boolean = s == s.reverse

    val palindromes = for {
      x <- 100 until 1000
      y <- x until 1000
      p = x * y
      if isPalindrome(p.toString)
    } yield p

    palindromes.toList
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail.foreach(line => printResult(line))
  }

  def printResult(line: String): Unit = {
    val limit = line.toInt
    val result = largestPalindromicNumber(limit)
    println(result)
  }

}