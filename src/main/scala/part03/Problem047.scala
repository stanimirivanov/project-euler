package part03

/**
 * Distinct primes factors
 * Problem 47
 *
 * The first two consecutive numbers to have two distinct prime factors are:
 *
 * 14 = 2 x 7
 * 15 = 3 x 5
 *
 * The first three consecutive numbers to have three distinct prime factors are:
 *
 * 644 = 2^2 x 7 x 23
 * 645 = 3 x 5 x 43
 * 646 = 2 x 17 x 19.
 *
 * Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?
 */
object Problem047 extends App {

  lazy val N: Stream[BigInt] = BigInt(1) #:: N.map(_ + 1)

  lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)
  
  lazy val primes = BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))
  
  def primeFactors(n: BigInt): Set[BigInt] = n match {
    case Big(1) => Set()
    case n => {
      val f = primes.find(n % _ == 0).get
      primeFactors(n / f) + f
    }
  }
  
  val result = N.tails.find(_.take(4).forall(primeFactors(_).size == 4)).get.head
  println(result)
  
  // extractor for BigInt
  object Big {
    def unapply(n: BigInt) = Some(n.toInt)
  }
}