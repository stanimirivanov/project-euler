package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem007Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem007.nthPrime(10001) }
    assert(result === 104743)
  }

  "Problem" should "pass max value" in {
    val result = time { Problem007.nthPrime(10000) }
    assert(result === 104729)
  }

}