package part02

/**
 * Pandigital products
 * Problem 32
 *
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once;
 * for example, the 5-digit number, 15234, is 1 through 5 pandigital.
 *
 * The product 7254 is unusual, as the identity, 39 x 186 = 7254, containing multiplicand, multiplier,
 * and product is 1 through 9 pandigital.
 *
 * Find the sum of all products whose multiplicand/multiplier/product identity
 * can be written as a 1 through 9 pandigital.
 *
 * HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
 */
object Problem032 extends App {

  def digits(n: Int): Seq[Int] = n.toString.toList.map(_.asDigit)

  def isPandigital(a: Int, b: Int, n: Int): Boolean =
    (digits(a) ++ digits(n) ++ digits(b)).sortWith(_ < _) == (1 to 9)

  def hasPandigitalFactor(n: Int) =
    (2 until Math.sqrt(n).toInt).filter(n % _ == 0).exists(a => isPandigital(a, n / a, n))

  def result = (1000 until 10000).filter(n => hasPandigitalFactor(n)).sum
  println(result)

}