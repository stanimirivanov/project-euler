package part03

/**
 * Prime permutations
 * Problem 49
 *
 * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways:
 * (i) each of the three terms are prime, and,
 * (ii) each of the 4-digit numbers are permutations of one another.
 *
 * There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but
 * there is one other 4-digit increasing sequence.
 *
 * What 12-digit number do you form by concatenating the three terms in this sequence?
 */
object Problem049 extends App {

  lazy val ODD: Stream[BigInt] = BigInt(1) #:: ODD.map(_ + 2)

  lazy val primes = BigInt(2) #:: ODD.filter(n => n.isProbablePrime(100))

  val candidates = primes.dropWhile(_ < 1000).takeWhile(_ < 10000).toSet

  def samePermutations(n: BigInt, m: BigInt): Boolean =
    n.toString.sorted == m.toString.sorted

  val result = for {
    x <- candidates
    y <- candidates
    if y > x && samePermutations(x, y)
    z = y * 2 - x
    if candidates.contains(z) && samePermutations(y, z)
  } yield List(x, y, z).mkString
  
  println(result)
}