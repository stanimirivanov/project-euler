package part01

/**
 * Largest product in a series
 * Problem 8
 *
 * Find the greatest product of K consecutive digits in the N digit number.
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler008
 */
object Problem008 {

	def largestConsecutiveProduct(digitString: String, size: Int): Long =
		digitString.map(_.asDigit.toLong).sliding(size).map(_.product).max

	def main(args: Array[String]) {
		val lines = io.Source.stdin.getLines().toArray
		lines.tail.grouped(2) foreach { printResult }
	}

	def printResult(lines: Array[String]): Unit = {
		val Array(_, k) = lines(0).split(" ").map(_.toInt)
		val digitString = lines(1)
		val result = largestConsecutiveProduct(digitString, k)
		println(result)
	}
  
}