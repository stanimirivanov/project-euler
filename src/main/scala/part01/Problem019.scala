package part01

/**
 * Counting Sundays
 * Problem 19
 *
 * You are given the following information, but you may prefer to do some research for yourself.
 *
 * - 1 Jan 1900 was a Monday.
 * - Thirty days has September, April, June and November.
 * - All the rest have thirty-one,
 * - Saving February alone,
 * - Which has twenty-eight, rain or shine.
 * - And on leap years, twenty-nine.
 * - A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
 *
 * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */
object Problem019 extends App {

  def isLeapYear(year: Int): Boolean = 
    (year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0

  val startDayOfMonth: (Int) => (Int) => Int = year => month => month match {
    case 1 => if (year == 1900) 1 else ((startDayOfMonth(year - 1)(12) + 31) % 7)
    case 3 => (startDayOfMonth(year)(2) + (if (isLeapYear(year)) 29 else 28)) % 7
    case m if m == 5 || m == 7 || m == 10 || m == 12 => (startDayOfMonth(year)(m - 1) + 30) % 7
    case m => (startDayOfMonth(year)(m - 1) + 31) % 7
  }

  val result = (1901 to 2000).map(y => (1 to 12).map(startDayOfMonth(y)).count(_ == 0)).sum
  println(result)

}