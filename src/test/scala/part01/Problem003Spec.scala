package part01

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import utils.Profiler

@RunWith(classOf[JUnitRunner])
class Problem003Spec extends FlatSpec with Profiler {

  "Problem" should "pass test case" in {
    val result = time { Problem003.factors(600851475143L).max }
    assert(result === 6857)
  }

  "Problem" should "pass max value" in {
    val result = time { Problem003.factors(1000000000000L).max }
    assert(result === 5)
  }

}