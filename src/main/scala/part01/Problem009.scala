package part01

/**
 * Special Pythagorean triplet
 * Problem 9
 *
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 *
 * a^2 + b^2 = c^2
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 *
 * There exists exactly one Pythagorean triplet for which a + b + c = N.
 * Find maximum possible value of abc among all such Pythagorean triplets.
 * If there is no such Pythagorean triplet print ?1.
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler009
 */
object Problem009 {

  def tripletProduct(limit: Int): Option[Int] = {
    val triplets = for {
      a <- 1 to limit/2
      b <- 1 to a
      c = math.sqrt(a*a + b*b)
      if (a + b + c == limit)
    } yield a * b * c

    triplets match {
      case Seq() => None
      case _   => Some(triplets.max.toInt)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toArray
    lines.tail foreach { printResult }
  }

  def printResult(line: String): Unit = {
    val limit = line.toInt
    val result = tripletProduct(limit)
    result match {
      case Some(x) => println(x)
      case None    => println(-1)
    }
  }

}