package part01

import scala.collection.immutable.NumericRange

/**
 * Sum square difference
 * Problem 6
 *
 * The sum of the squares of the first ten natural numbers is,
 *
 * 1^2 + 2^2 + ... + 10^2 = 385
 * The square of the sum of the first ten natural numbers is,
 *
 * (1 + 2 + ... + 10)^2 = 55^2 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.
 *
 * Find the difference between the sum of the squares of the first one N natural numbers and the square of the sum.
 *
 * @see https://www.hackerrank.com/contests/projecteuler/challenges/euler006
 */
object Problem006 {

  def sumSquareDifference(n: BigInt): BigInt = {
    val sum = n * (n + 1) / 2
    val squared = (n * (n + 1) * (2 * n + 1)) / 6
    sum * sum - squared
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    lines.tail foreach { printResult }
  }

  def printResult(line: String): Unit = {
    val n = BigInt(line)
    val result = sumSquareDifference(n)
    println(result)
  }
}