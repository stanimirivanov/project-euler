package part02

import scala.util._
import scala.io._

/**
 * Names scores
 * Problem 22
 *
 * Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names,
 * begin by sorting it into alphabetical order. Then working out the alphabetical value for each name,
 * multiply this value by its alphabetical position in the list to obtain a name score.
 *
 * For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
 * So, COLIN would obtain a score of 938 x 53 = 49714.
 *
 * What is the total of all the name scores in the file?
 */
object Problem022 extends App {
  val fileName = "names.txt"

  def readTextFile(fileName: String): Try[Array[String]] = {
    Try(Source.fromFile(fileName).mkString.replaceAll("\"", "").split(","))
  }

  def score(name: String, index: Int): Int =
    name.toList.map(_.toInt - 64).sum * (index + 1)

  def allScores(lines: Array[String]): Array[Int] =
    lines.sorted.zipWithIndex.map {
      case (name, i) => score(name, i)
    }

  readTextFile(fileName) match {
    case Success(lines) => println(allScores(lines).sum)
    case Failure(f) => println(f)
  }
}